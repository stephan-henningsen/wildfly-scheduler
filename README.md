# Scheduler Example #



## Build WAR ##

```
./gradlew :scheduler-service:war
```



## Run container ##

```
docker-compose up --build
```


## Test scheduler service ##

Observe log output.



## Test web service ##

```
curl http://localhost:10080/healthz
curl http://localhost:10080/debug/ping/hi-there
```

