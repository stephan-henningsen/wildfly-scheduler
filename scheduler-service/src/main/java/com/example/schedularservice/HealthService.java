package com.example.schedularservice;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/healthz")
public class HealthService {

    @GET
    public Response health() {
        return Response.ok().entity("I'm fine, thanks for asking! =) "
                + LocalDateTime.now(ZoneId.of("Europe/Copenhagen"))
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss")) + "\n")
                .build();
    }

}
