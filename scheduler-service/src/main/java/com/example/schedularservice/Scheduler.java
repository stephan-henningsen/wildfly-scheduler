package com.example.schedularservice;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Schedule;
import javax.ejb.Singleton;

@Singleton
public class Scheduler
{

    @Schedule(second = "*/6", minute = "*", hour = "*", persistent = false)
    public void doWork ()
    {
        Date currentTime = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        System.out.println("Scheduler.doWork() invoked at " + simpleDateFormat.format(currentTime));
    }

}