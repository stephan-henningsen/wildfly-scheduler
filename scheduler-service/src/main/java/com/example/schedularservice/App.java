package com.example.schedularservice;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("")
public class App extends Application
{
    @Override
    public Set<Class<?>> getClasses ()
    {
        Set<Class<?>> classes = new HashSet<>(super.getClasses());

        classes.add(HealthService.class);
        classes.add(DebugService.class);

        return classes;
    }

}
