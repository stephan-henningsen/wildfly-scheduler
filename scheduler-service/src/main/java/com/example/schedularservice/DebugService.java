package com.example.schedularservice;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import static javax.ws.rs.core.Response.Status.OK;

@Path("/debug")
public class DebugService
{

    @GET
    @Path("/ping")
    public Response ping ()
    {
        return ping("default-input");
    }

    @GET
    @Path("/ping/{input}")
    public Response ping ( @PathParam("input") String pingInput )
    {
        return Response.status(OK).entity("pong[" + pingInput + "]").build();
    }

    @GET
    @Path("/exception")
    public String getException ()
    {
        throw new IllegalStateException("Keep calm, this is a test!");
    }

    @GET
    @Path("/string")
    public String getString ()
    {
        return "Hello, World";
    }

    @GET
    @Path("/int-as-string/{s}")
    public Integer getIntAsString ( @PathParam("s") String s )
    {
        return Integer.parseInt(s);
    }

    @GET
    @Path("/int-as-int/{i}")
    public int getIntAsInt ( @PathParam("i") int i )
    {
        return i;
    }


    @GET
    @Path("/soe/{stringOrErrorCode}")
    public String getIntOrResponseError ( @PathParam("stringOrErrorCode") String stringOrErrorCode )
    {
        try {
            int errorCode = Integer.parseInt(stringOrErrorCode);
            throw new WebApplicationException("Here is your errorCode, sir!", errorCode);
        } catch ( NumberFormatException ex ) {
            return stringOrErrorCode;
        }
    }

    @GET
    @Path("/error/{errorCode}")
    public Response getError ( @PathParam("errorCode") int errorCode )
    {
        return Response.status(errorCode).build();
    }

    @GET
    @Path("/readme")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getREADME ()
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        InputStream inputStream = classLoader.getResourceAsStream("README.md");

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write ( OutputStream out ) throws IOException, WebApplicationException
            {
                Writer writer = new BufferedWriter(new OutputStreamWriter(out));

                byte[] buffer = new byte[1024 * 10];
                int len;
                while ( (len = inputStream.read(buffer)) != -1 ) {
                    out.write(buffer, 0, len);
                }

                writer.flush();
            }
        };

        return Response.ok(stream).build();
    }
}
